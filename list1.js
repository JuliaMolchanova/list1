var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30,'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

console.log('Список студентов:');
for (var i = 0, imax = studentsAndPoints.length; i < imax; i += 2) {
	console.log('Студент ' +studentsAndPoints[i]+ ' набрал ' +studentsAndPoints[i+1]+ ' баллов');
}


var maxIndex = -1, max;
for (var i = 0, imax = studentsAndPoints.length; i < imax; i += 2) {
    if (maxIndex < 0 || studentsAndPoints[i+1] > max) {
	maxIndex = i + 1;
	max = studentsAndPoints[i+1];
	}
}
console.log('Студент, набравший максимальный балл:');
console.log('Студент '+studentsAndPoints[maxIndex - 1]+ ' имеет максимальный балл ' +max);


console.log('В группе появились новые студенты Николай Фролов и Олег Боровой:');
studentsAndPoints.push('Николай Фролов', 0, 'Олег Боровой', 0);
for (var i = 0, imax = studentsAndPoints.length; i < imax; i += 2) {
	console.log('Студент ' +studentsAndPoints[i]+ ' набрал ' +studentsAndPoints[i+1]+ ' баллов ');
}

/*  for (var i = 0, imax = studentsAndPoints.length; i < imax; i += 2) {
	if (studentsAndPoints[i] === 'Антон Павлович') {
		studentsAndPoints[i+1] += 10;
	}
	else if (studentsAndPoints[i] === 'Николай Фролов') {
		studentsAndPoints[i+1] += 10;
	}
}
*/

var antonPavlovich = studentsAndPoints.indexOf('Антон Павлович');
if (antonPavlovich != -1) {
	studentsAndPoints[antonPavlovich+1] += 10;
}

var nicolayFrolov = studentsAndPoints.indexOf('Николай Фролов');
if (nicolayFrolov != -1) {
	studentsAndPoints[nicolayFrolov+1] += 10;
}

console.log('Антон Павлович и Николай Фролов набрали по 10 баллов');
for (var i = 0, imax = studentsAndPoints.length; i < imax; i += 2) {
	console.log('Студент ' +studentsAndPoints[i]+ ' набрал ' +studentsAndPoints[i+1]+ ' баллов ');
}



console.log('Студенты, не набравшие баллов:')
for (var i = 0, imax = studentsAndPoints.length; i < imax; i += 2) {
	if (studentsAndPoints[i+1] === 0) {
		console.log(studentsAndPoints[i]);
	}
}


console.log('Студенты, набравшие 0 баллов, удалены из списка:');
for (var i = 0, imax = studentsAndPoints.length; i < imax; i += 2) {
	if (studentsAndPoints[i+1] === 0) {
		studentsAndPoints.splice(i,2)
		i -= 2;
	}
}

for (var i = 0, imax = studentsAndPoints.length; i < imax; i += 2) {
	console.log('Студент ' +studentsAndPoints[i]+ ' набрал ' +studentsAndPoints[i+1]+ ' баллов ');
}
